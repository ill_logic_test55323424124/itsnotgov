<!-- title: I Can't Believe It's Not Government! -->
<!-- redirect: /home -->

![I Can't Believe It's Not Government](/images/logo.png)
<h1 class="site-title">Serving up stories from a free society</h1>
<p>They say free people can't get the job done. They're not impressed by theory. They want to see <i>real life</i> examples. So we're collecting them, organized by "government function". <a href="/about">Learn more</a> about how you can help.</p>
