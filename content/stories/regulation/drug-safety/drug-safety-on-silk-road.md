<!-- title: Drug Safety on Silk Road -->

<!-- redirect: /w/c/Regulation/DrugSafety/DrugSafetyOnSilkRoad -->

The government is generally considered necessary for safety testing of pharmaceuticals or other drugs. Some organizations may perform or commission tests of their own drugs, but a good case can be made that the government has a strong influence in the matter. For instance in the United States, drug manufacturers need to pass [FDA](http://en.wikipedia.org/wiki/Food_and_Drug_Administration) scrutiny. What about an organization, or community, with zero government oversight? Would they have any incentive to test their own drugs, or drugs provided by others?

The [Silk Road](http://en.wikipedia.org/wiki/Silk_Road_(marketplace)) was a black market website on the [TOR Network](http://en.wikipedia.org/wiki/Tor_%28anonymity_network%29), primarily known for facilitating the sale of narcotics. Silk Road anonymously connected buyers and sellers of products, mostly contraband. All products were purchased in [Bitcoin](http://en.wikipedia.org/wiki/Bitcoin), and [were shipped][economist_article] to the customers.

[TOC]

# Vendor Ratings

Customers who purchased drugs were able to [give ratings][cmu_study] on a scale from 1 to 5, as well as post detailed reviews and comments. Some of these reviews included claims to have done chemical testing.

From the [Silk Road Archive on antilop.cc][bad_amphetamine_review]:

> Pharmington Rex sent me pills that don't even test positive for amphetamine. I contacted him and told him about this, and offered to show him a video of me testing the drug when he claimed I'm probably scamming him, and he just videos could be easily faked. I then offered to do it live on skype or similar(chatroulette even) and he then says that would risk my anonymity. I still offered to do it, and risking my anonymity. He then accused me of potentially being a cop.

The comment goes on to link to corroborating accounts from other customers.

## LSD Avengers

An independent group known as the [LSD Avengers][theverge_lsd_avengers] took it upon themselves to become renowned on Silk Road as [LSD](http://en.wikipedia.org/wiki/Lysergic_acid_diethylamide) testers. They purchased LSD from various venders on the site, and posted reviews. The tests they claimed to conduct included a chemical test, largely to distinguish legitimate LSD from [research chemicals](http://en.wikipedia.org/wiki/Designer_drug) that had been passed off as LSD. Some of their reviews included photographs of these chemical tests. Pending success of the chemical test, they claimed they would also sample it for consumption.

# Incentives

Since the Silk Road was entirely illegal, and ultimately shut down by the government, it seems implausible that the government influenced or assisted it in any way toward safety testing the drugs that were for sale. The same could be said for any associated third party drug testing group.

# Results

Because of the secretive nature of Silk Road, it is difficult to establish a cause-effect relationship between the various methods of self-regulation and drug quality or customer satisfaction. However we may still find evidence of high quality of drugs, or of customer satisfaction, which maintain the plausibility of the effectiveness of the self-regulation.

Despite the anonymity of the sellers, according to a [Carnegie Mellon University study][cmu_study], customers on average gave very high reviews for the products they recived. However, the study's author cautions against drawing a strong conclusion, since high ratings are very common on similar sites such as Ebay.

A [survey][drug_user_survey] among drug users in United Kingdom, Austrailia, and the United States, found that, among those who chose to purchase from Silk Road, higher quality drugs was one of the leading reasons for doing so (72-77%). Lower quality of drugs were not among the leading reasons for not choosing Silk Road, though sufficient access to drugs was a leading reason. The study's conclusion notes that this accords with greater online commerce trends.

# Sources

## References

* [The LSD Avengers, Silk Road’s self-appointed drug inspectors, announce retirement][theverge_lsd_avengers]
    * Somewhat biased article, however supplies the facts that the LSD Avengers existed, and that they posted photographs of their tests
* [Traveling the Silk Road: A measurement analysis
of a large anonymous online marketplace][cmu_study]
    * See 4.3 "Customer Satisfaction", page 14
    * Nicolas Christin of Carnegie Mellon University
* A [survey of drug users][drug_user_survey] from the United Kingdom, Austrailia, and the United States, regarding their familiarity with and opinion of Silk Road
* [Economist Article][economist_article] about Silk Road

## Testimonials

* From Silk Road archive on antilop.cc:
    * [Bad review of amphetamine dealer][bad_amphetamine_review], including claims of having conducted a chemical test

## Other Resources

* [Traveling the Silk Road: Datasets][cmu_study_data]

## Further Reading

* [Buying Your Drugs Online Is Good for You](http://www.vice.com/read/silk-road-is-good-for-you) - Vice.com

[theverge_lsd_avengers]: http://www.theverge.com/2013/10/14/4828448/silk-road-lsd-avengers-drug-inspectors
[cmu_study]: http://www.andrew.cmu.edu/user/nicolasc/publications/TR-CMU-CyLab-12-018.pdf
[cmu_study_data]: https://arima.cylab.cmu.edu/sr/
[bad_amphetamine_review]: http://antilop.cc/sr/users/libertas/html/Libertas_posts_page_0053_start_0780.html
[economist_article]: http://www.economist.com/blogs/economist-explains/2013/08/economist-explains-11
[drug_user_survey]: http://onlinelibrary.wiley.com/doi/10.1111/add.12470/abstract