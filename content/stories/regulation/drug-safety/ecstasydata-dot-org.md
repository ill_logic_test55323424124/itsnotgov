<!-- title: EcstasyData.org -->

<!-- redirect: /w/c/Regulation/DrugSafety/ecstasydataorg -->

[EcstacyData.org][website] is a website that performs laboratory analysis on psychoactive substances submitted to them, to make the data publicly available for the purpose of harm reduction and research. It was [started in 2001][website_about]. It [is a project][website_about] of the [Erowid Center][erowid], a popular drug information resource. It [has a license][website_faq] from the Drug Enforcement Administration of the United States.

# Funding

According to its site (as of August 2018), EcstacyData [receives funding][website_about] from three organizations: Erowid Center, [Isomer Design][isomer_design], and [DanceSafe][dancesafe] (which is also in turn affiliated with Erowid). Erowid Center is fully [funded by donations][erowid_funding]. Isomer Design's funding sources are unclear, but they have [only funded][website_about] EcstasyData since 2013. DanceSafe is also [funded by donations][dancesafe_funding] (according to an article on their site from 2014).

# Credibility

While Erowid [describes itself as][erowid_about] a "non-judgmental" source on the topic of psychoactive drugs, [Project Know][project_know], an information resource for those dealing with addition, [used EcstacyData as a source][project_know_pill] for an analysis.

# Sources

## References

EcstacyData.org:

* [Home][website]
* [About][website_about]
* [FAQ][website_faq]

Erowid Center:

* [Home][erowid]
* [About][erowid_about]
* [Funding][erowid_funding]

DanceSafe:

* [Home][dancesafe]
* [Funding][dancesafe_funding]

Project Know:

* [Home][project_know]
* [Study][project_know_pill] that references EcstacyData.org

## Other Sources

* [Rolling Stone article][dancesafe_rolling_stone] about DanceSafe
* [Vice article][vice] about EcstacyData and Project Know

[website]: http://www.ecstasydata.org/
[website_faq]: https://www.ecstasydata.org/faq.php
[website_about]: https://www.ecstasydata.org/about.php

[erowid]: https://www.erowid.org
[erowid_funding]: https://www.erowid.org/general/about/about_funding_contrib17.php
[erowid_about]: https://erowid.org/general/about/

[dancesafe]: https://www.dancesafe.org/
[dancesafe_funding]: https://dancesafe.org/join-the-untz-and-dancesafe/

[isomer_design]: https://isomerdesign.com/

[project_know]: https://www.projectknow.com
[project_know_pill]: https://www.projectknow.com/discover/jagged-little-pill/

[dancesafe_rolling_stone]: https://www.rollingstone.com/music/music-news/meet-the-people-who-want-to-make-it-safer-to-take-drugs-at-festivals-49560/
[vice]: https://noisey.vice.com/en_us/article/rmjvwn/whats-really-in-your-ecstasy