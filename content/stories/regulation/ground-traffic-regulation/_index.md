<!-- title: Ground Traffic Regulation -->

<!-- redirect: /w/c/Regulation/GroundTrafficRegulation -->

![Traffic](/images/stories/regulation/ground-traffic-regulation/traffic.jpg)

In a libertarian world, all roads would be [privately owned and financed](/infrastructure/roads). Just as a restaurant owner in a libertarian world could set the rules for their own restaurant, so could a road owner create their own rules of the road. But this isn't a very interesting answer to the question of road regulation. There are some remaining questions. How do we know a road owner will create good rules? How will drivers deal with every road owner potentially setting different rules? Can the drivers be regulated in other ways?

# Theory

The libertarian approach to this question is generally that the roads owners have an incentive to find optimal rules, to minimize accidents and maximize traffic flow, to attract more customers. Creating rules that are drastically different from other roads could cause confusion, irritation, or even accidents, so road owners similarly have an incentive to avoid this. However, they will have the incentive to vary their rules *somewhat*, for the purpose of experimentation, which helps with forming more efficient and effective regulations over time.

There are potential gaps in knowledge here between small road owners, but there are opportunities for larger coordinating forces, such as insurance companies, or associations between multiple road owners. In this way, regulation could happen at a larger scale.

# Real Life Examples

Insurance companies play a role in traffic safety today. To cover risk and cost of insurance claims, insurance companies need to set premiums sufficiently high, but to remain competitive they must set them sufficiently low. In the search for lower prices that can still cover risks, insurance companies search for conditions that are safer than others, and offer cheaper policies under those conditions, which in turn encourages policy holders to choose safer alternatives.

Insurance companies fund safety tests of automobiles, in order to be able to charge lower rates for safer cars. They have also had some effect on [shaping traffic policy](InsuranceInstituteForHighwaySafetyRoadRegula) in the United States. Insurance companies have even started offering lower rates to drivers who agree to [have their driving habits electronically monitored](AutoInsuranceTelematics), in order to offer lower premiums for safer drivers, creating an incentive for drivers to be safer. Insurance companise also play a part in [dispute resolution in traffic incidents](/legal-system/courts-arbitration).
