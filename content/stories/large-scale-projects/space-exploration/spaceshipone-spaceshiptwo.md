<!-- title: SpaceShipOne, SpaceShipTwo -->

SpaceShipOne was a [privately funded spacecraft][space], designed by aerospace engineer [Burt Rutan][burt_wikipedia], that achieved suborbital spaceflight. It [was the first][space] manned spacecraft not funded by the government. It won the $10 million [Ansari XPRIZE][ansari], which required building a spacecraft that could be reused within two weeks, among other requirements. After one of its flights, the pilot Mike Melvill [stood atop the craft][spacereview] and held a sign handed to him by a spectator which read "SpaceShipOne, Government Zero".

Funding [came from][space] Paul Allen, co-founder of Microsoft, [who supplied][wired] $25 million. Richard Branson [licensed the technology][ansari] for Virgin Galactic and [funded its successor][space] SpaceShipTwo five years later.

# Sources

## References

[space]: https://www.space.com/16769-spaceshipone-first-private-spacecraft.html
* [Article from Space.com][space]
[ansari]: https://ansari.xprize.org/prizes/ansari
* [Ansari XPRIZE award][ansari]
[wired]: https://www.wired.com/2004/10/spaceshipone-wins-the-x-prize/
* [Wired Article][wired] announcing that SpaceShipOne won the Ansari XPRIZE
[spacereview]: http://www.thespacereview.com/article/1649/1
* [Article from The Space Review][spacereview]

## Other Sources

[burt_wikipedia]: https://en.wikipedia.org/wiki/Burt_Rutan
* [Burt Rutan][burt_wikipedia] - Wikipedia Article
[spaceshipone_wikipedia]: https://en.wikipedia.org/wiki/SpaceShipOne
* [SpaceShipOne][spaceshipone_wikipedia] - Wikipedia Article
[spaceshiptwo_wikipedia]: https://en.wikipedia.org/wiki/SpaceShipTwo
* [SpaceShipTwo][spaceshiptwo_wikipedia] - Wikipedia Article
[virgin_galactic]: https://www.virgingalactic.com/
* [Virgin Galactic][virgin_galactic] - Richard Branson's commercial spaceflight company
