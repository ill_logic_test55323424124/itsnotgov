<!-- title: St. Jude Children's Research Hospital -->

[St. Jude Children's Research Hospital][site_main] is a hospital and research center in Memphis, Tennessee that treats children with [certain catastrophic diseases][site_mission]. Patients are seen [at no cost][site_mission] to their family.

# Government Involvement

According to their website, St. Jude [receives grants][site_facts] from the National Institutes of Health (part of the US government). However, [approximately 75%][site_unique] of its funding comes from donors through the American Lebanese Syrian Associated Charities (ALSAC), and [more comes][site_facts] from insurance recoveries and investments. [According to Charity Navigator][charity_navigator], it appears that 5.7% of their funding in FYE 2017 comes from government grants. (However it is a bit ambiguous, as the title of the report refers to ALSAC, which is only part of their funding.)

In 2018 they also [received $36 million][biz] from the Tennessee State Funding Board to expand their campus.

# Results

[According to their website][site_facts], St. Jude sees approximately 7,500 patients per year.

# Sources

* St. Jude's website:
[site_main]: https://www.stjude.org/
    * [Main][site_main]
[site_mission]: https://www.stjude.org/about-st-jude.html?sc_icid=us-mm-missionstatement#mission
    * [Mission Statement][site_mission]
[site_unique]: https://www.stjude.org/about-st-jude/unique-operating-model.html
    * ["Our Unique Operating Model"][site_unique]
[site_facts]: https://www.stjude.org/media-resources/media-tools/facts.html
    * [Facts page][site_facts]
[biz]: https://www.bizjournals.com/memphis/news/2018/09/13/latest-stategrant-for-st-jude-expansion-brings.html
* [Article][biz] in The Memphis Business Journal about St. Jude receiving a Tennessee state grant to expand their campus.
[charity_navigator]: https://web.archive.org/web/20181021185916/https://www.charitynavigator.org/index.cfm?bay=partners.summary&ein=351044585
* [Charity Navigator][charity_navigator] page for St. Jude and ALSAC

# Other Sources

* More from St. Jude's Website:
[site_financials]: https://www.stjude.org/about-st-jude/financials.html
    * [Financials][site_financials]
[site_nih]: https://www.stjude.org/media-resources/news-releases/2015-medicine-science-news/st-jude-to-receive-nih-grants.html
    * Story about [receiving NIH grants][site_nih]
[wikipedia]: https://en.wikipedia.org/wiki/St._Jude_Children%27s_Research_Hospital
    * [Wikipedia][wikipedia]

[comment]: <> (TODO: They make a claim about survival rates, but let's find a third party source. There's the claim that cancer survival improved in general, and there's the claim about their hospital. The former is good to know if it can be shown that it had to do with their hospital, but that's hard to show (and also maybe it belongs in the research section of this website). The latter is good to know just to validate that the free service they receive is still of good quality.)
