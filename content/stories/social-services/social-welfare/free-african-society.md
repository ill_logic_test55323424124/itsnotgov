<!-- title: Free African Society -->

[The Free African Society][ph_enc] was a [mutual aid society][wikipedia_bs] in the United States, the [first for free black Americans][britannica] in Philadelphia. [Formed in 1787][britannica] by two black American preachers. It was [ultimately nondenominational][britannica], but it was religiously inspired. The [membership was split][britannica] between Methodist and Episcopal faiths.

Their [preamble][pbs_preamble_articles]:

> (12th, 4th mo., 1778] -- Whereas, Absalom Jones and Richard Allen, two men of the African race, who, for their religious life and conversation have obtained a good report among men, these persons, from a love to the people of their complexion whom they beheld with sorrow, because of their irreligious and uncivilized state, often communed together upon this painful and important subject in order to form some kind of religious society, but there being too few to be found under the like concern, and those who were, differed in their religious sentiments; with these circumstances they labored for some time, till it was proposed, after a serious communication of sentiments, that a society should be formed, without regard to religious tenets, provided, the persons lived an orderly and sober life, in order to support one another in sickness, and for the benefit of their widows and fatherless children.

Membership [dues were][ph_enc] one shilling per month. [Benefits included][ph_enc] burial services, financial aid for families with deceased members, finding apprenticeships or paying school tuition for children, and unemployment and sickness assistance. Membership was [contingent on][ph_enc] good behavior. [Per their Articles][pbs_preamble_articles]:

> And it is further agreed, that no drunkard nor disorderly person be admitted as a member...

Help was contingent on the misfortune being due to bad luck rather than bad behavior. Again [per their Articles][pbs_preamble_articles]:

> ...then to hand forth to the needy of this Society, if any should require, the sum of three shillings and nine pence per week of the said money: provided, this necessity is not brought on them by their own imprudence.

The society [disbanded][britannica] in 1794. [According to Encyclopædia Britannica][britannica], it was due to debts incurred while supporting people afflicted by Yellow Fever the previous year. According to the [Encyclopedia of Greater Philadelphia][ph_enc], they disbanded due to a religious schism, particularly because of Quaker influence. However, they inspired [more than one hundred][ph_enc] mutual aid societies of free black Americans in the area by 1838.

# Sources

# References

[ph_enc]: https://philadelphiaencyclopedia.org/archive/free-african-society/
* [Encyclopedia of Greater Philadelphia article][ph_enc] about the Free African Society
[britannica]: https://www.britannica.com/topic/Free-African-Society
* [Encyclopædia Britannica article][britannica] about the Free African Society
[pbs_preamble_articles]: http://www.pbs.org/wgbh/aia/part3/3h465t.html
* [Preamble and Articles of the Free African Society][pbs_preamble_articles] from PBS

# Other Sources
* Wikipedia articles
[wikipedia_fas]: https://en.wikipedia.org/wiki/Free_African_Society
    * The [Free African Society][wikipedia_fas]
[wikipedia_bs]: https://en.wikipedia.org/wiki/Benefit_society
    * [Benefit Societies][wikipedia_bs]
[wikipedia_ma]: https://en.wikipedia.org/wiki/Mutual_aid_(organization_theory)
    * [Mutual Aid theory][wikipedia_ma]
