<!-- title: Rotary International: Spearheading the Effort to Eradicate Polio -->

[Rotary International][website] is charity organization largely comprising [business and professional leaders][oxford]. In 1979, Rotary began a project to [vaccinate children in the Philippines][oxford] against polio. By 1985, they [had an expanded program][oxford] they named PolioPlus with the goal of eradicating polio from the planet entirely, as had been done with [smallpox](https://en.wikipedia.org/wiki/Smallpox). In 1988, they [were joined][oxford] by government organizations such as World Health Organization, UNICEF, and the Centers for Disease Control for a joint campaign called the Global Polio Eradication Initiative. In 2007, the Bill and Melinda Gates foundation [also joined the effort][cnbc].

[According to Dr Margaret Chan][oxford], Director-General (as of 2017) of the [World Health Organization](https://en.wikipedia.org/wiki/World_Health_Organization) (part of the United Nations):

> Rotary International is the top private sector contributor and volunteer arm of the eradication initiative. The 1.2 million Rotarians envisioned a polio-free world, and then challenged governments and health agencies to pursue this vision.

The actual extent of their success independent of government assistance is difficult to determine.

# Results

## Scope

In 1979, Rotary [granted $760000][oxford] to immunize 6 million children in the Philippines. Starting around 1985, members raised $247 million within three years for the new PolioPlus program. It [does not seem][who_graph] that the efforts before 1988 (when government agencies stepped in) made a noticeable impact in polio cases worldwide. However a paper in The Journal of Infectious Diseases (Oxford University Press) [refers to][oxford] the initial immunization project in the Philippines as a "success", which precipitated the large expansion into other countries with much more money spent. However, actual numbers on reduction of rates in those areas in the years before government agencies stepped in (1979-1988) are hard to find, and would clarify the scope of Rotary's success without government help here.

By 1988, however, they [decided][oxford] that they needed help from other organizations. That help [came from][oxford] government organizations such as UNICEF and the World Health Organization. Though in 2007 they also received help from the [Bill and Melinda Gates foundation](https://en.wikipedia.org/wiki/Bill_%26_Melinda_Gates_Foundation), including a [financial contribution][cnbc] of almost $3 billion.

## Funding

One of the big questions involved with the eradication of a disease is one of funding. It would be good to find a clear statement from Rotary International as to its sources of funding their portion of the polio eradication effort (i.e., separate from the contributions of other organizations).

It is difficult to find definitive statements about sources of funding, but CNBC refers to PolioPlus as "[private-sector][cnbc]". Members of Rotary have donated a [total of $1.6 billion][oxford] toward the eradication of polio as of 2017.

On the other hand, at least in the case of Rotary Foundation (Canada), the government [provides some funding][clubrunner]. It is unclear if this funding is related to the polio eradication efforts, but of course money is fungible so it is good to point out.

## Local Cooperation and Coordination

The other questions that remain are local issues of the administration of vaccines. It would be good to determine the history of mandating vaccinations across the various countries, from the beginning of the project to the day each country was declared polio-free.

There is also the question of the government assisting with coordination on the ground. It is at least clear that Rotary needed to [coordinate][oxford] with the government of the Philippines in their initial immunization project. There were also [national immunization days][oxford] as part of PolioPlus.

# Sources

## References

* [Rotary International][website] - Official Website
* [Rotary’s PolioPlus Program: Lessons Learned, Transition Planning, and Legacy][oxford] (from The Journal of Infectious Diseases, [Oxford University Press](https://en.wikipedia.org/wiki/Oxford_University_Press))
* [Government of Canada Update][clubrunner] - regarding Rotary Foundation (Canada), via [Club Runner](https://www.crunchbase.com/organization/club-runner)
* [Bill Gates: For polio the endgame is near][cnbc] - CNBC
* [A graph of annual global reported cases of polio][who_graph] - World Health Organization

## Other Sources

* [Polio: Timeline][timeline]
* Wikipedia articles:
    * [Rotary International][wikipedia_rotary]
    * [Poliomyelitis][wikipedia_polio] (aka polio)
    * [Poliomyelitis Eradication][wikipedia_polio_eradication]

## Further Viewing

* [Historic Moments: How Rotary’s efforts to eradicate polio began][video] - on Vimeo

[website]: https://www.rotary.org
[wikipedia_rotary]: https://en.wikipedia.org/wiki/Rotary_International
[wikipedia_polio]: https://en.wikipedia.org/wiki/Poliomyelitis
[wikipedia_polio_eradication]: https://en.wikipedia.org/wiki/Poliomyelitis_eradication
[oxford]: https://academic.oup.com/jid/article/216/suppl_1/S355/3935037
[clubrunner]: https://portal.clubrunner.ca/100984/Stories/government-of-canada-update
[timeline]: http://amhistory.si.edu/polio/timeline/index.htm
[video]: https://vimeo.com/31740127
[cnbc]: https://www.cnbc.com/2017/10/24/bill-gates-humanity-will-see-its-last-case-of-polio-this-year.html
[who_graph]: http://www.who.int/immunization/monitoring_surveillance/burden/vpd/surveillance_type/active/Polio_coverage.jpg
