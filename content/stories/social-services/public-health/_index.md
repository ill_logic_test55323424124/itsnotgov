<!-- title: Public Health -->

From a libertarian perspective, the concept of "Public Health" is in some ways superfluous. The health of the public is simply an aggregate of the health of each individual. However, there are special considerations when it comes to issues such as the spread of disease. Certain projects inherently concern themselves with entire populations.
