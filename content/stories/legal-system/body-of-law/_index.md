<!-- title: Body of Law -->

<!-- redirect: /w/c/LegalSystem/BodyOfLaw -->

<!--

This is not ready to be presented. Commenting it out for now.

The question in play here is, in a system where dispute resolution is done in a decentralized manner, how will a standard set of "laws" (expectations for the behavior of members of society) evolve? As it turns out, this does tend to happen.

Perhaps this doesn't need to be its own section. It may be part of [Courts / Arbitration / Dispute Resolution](/w/c/LegalSystem/CourtsArbitration/).

Basis for investigation:

https://scholarship.law.unc.edu/cgi/viewcontent.cgi?article=4556&context=nclr

https://ecommons.luc.edu/luc_theses/1050/
-->
