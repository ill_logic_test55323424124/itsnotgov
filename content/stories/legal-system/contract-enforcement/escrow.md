<!-- title: Escrow -->

<!-- redirect: /w/c/LegalSystem/contract_enforcement/escrow -->

There are cases where two parties who do not have ultimate trust in each other would like to engage in a transaction that requires disbursements of assets when certain conditions are met. In such a case, it is possible to use a service called [Escrow][investopedia] provided by a trusted third party. The Escrow provider collects assets from one or both parties before the conditions are met, and disburses them as appropriate after they are met.

# Incentives

Reasons for trusting a given escrow service may include their reputation. However, in a scenario where the transaction and escrow service are under the traditional legal system (as opposed to, say, a [darknet market](https://en.wikipedia.org/wiki/Darknet_market)), the potential recourse of suing the escrow company if something were to go wrong is likely a factor.

# Results

Using an escrow service to manage assets in the agreement is a way of ensuring, with higher certainty than otherwise, that both sides of an agreement are met.

# Sources

## References

[Investopedia article][investopedia] on escrow.

## Other Resources

[Wikipedia article][wikipedia] on Escrow

[wikipedia]: https://en.wikipedia.org/wiki/Escrow
[investopedia]: https://www.investopedia.com/terms/e/escrow.asp