<!-- title: Guardian Angels -->

<!-- redirect: /w/c/Security/LocalDefenseSecurity/guardian_angels -->

The Guardian Angels is a [vigilante crime fighting organization][nydailynews]. It was started by Curtis Sliwa in 1979 in New York City, and were originally known as the Magnificent 13. Guardian Angels chapters eventually formed [around the United States][nydailynews]. They began gaining publicity by having patrols safeguarding dangerous subway routes, where police presence was lacking.

Originally, they did [not have a good relationship][nydailynews] with the New York City government and police department. However eventually, either due to proven results or mere publicity, the mayor of New York [conceded][nydailynews] that they were at least of some value:

> It's like chicken soup... Have they hurt? No. Have they helped? Yes.

In 1981, they were issued [official badges][nyt_recognition] from the city. Regarding this, [Sliwa said][nydailynews]:

> The badges make the New York Times crowd feel good... The people on the subways in Brooklyn never needed to see a badge. They were always happy enough just to see us get on the train.

It should be noted that in 1992, Curtis Sliwa [admitted to having fabricated][nyt_fake] several stories from the early days of the organization. His associates also claimed that there were [more confessions][nyt_fake] to come. It should also be noted that in 2018 they vowed to [raid a park][nypost_drug_raid] in Staten Island to perform citizens arrests of drug dealers, who is at least in principle are not engaged in violation of rights.

# Sources

## References

* [NY Daily News][nydailynews] article from 2017 about the history of the Guardian Angels
* [New York Timse article][nyt_recognition] from 1981 about the city officially giving recognition to the Guardian Angels
* [New York Times article][nyt_fake] from 1992 about Curtis Sliwa's confession of faking crimes
* [New York Post][nypost_drug_raid] article from 2018 about a vow to raid a park to stop drug deals.

## Other Sources

* [Wikipedia article][wikipedia]
* Vice News article: [On Patrol with the Guardian Angels, New York's Venerable Vigilantes][vice]
* [Official Website][website]

[wikipedia]: https://en.wikipedia.org/wiki/Guardian_Angels
[nydailynews]: http://www.nydailynews.com/new-york/guardian-angels-started-protecting-nyc-subways-article-1.804336
[nyt_recognition]: https://www.nytimes.com/1981/05/30/nyregion/the-city-guardian-angels-get-city-recognition.html
[nyt_fake]: https://www.nytimes.com/1992/11/25/nyregion/sliwa-admits-faking-crimes-for-publicity.html
[vice]: https://www.vice.com/en_us/article/ppxnan/on-patrol-with-the-guardian-angels-new-yorks-pseudo-vigilante-group
[nypost_drug_raid]: https://nypost.com/2018/06/22/guardian-angels-vow-to-raid-drug-infested-park-on-staten-island/
[website]: http://guardianangels.org/