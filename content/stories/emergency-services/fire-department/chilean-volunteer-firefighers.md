<!-- title: Chile's All-Volunteer Firefighers -->

<!-- redirect: /w/c/EmergencyServices/FireDepartment/ChileanVolunteerFirefighers -->

*This article is in-progress. It requires evidence for its principal claims.* 

Historically, firefighting services were provided by the community which required the service.  As communities grew in size, volunteer firefighting specialists became the norm.  After a time, most volunteer firefighting departments became subsidized and controlled by local governments.  This process has not been universal.

[TOC]

In Chile, firefighters are called "Bomberos". They are volunteers, ie they perform their services [for free][global_post_article]. There is a culture of [pride and community][santiago_times_article] surrounding the institution of unpaid volunteer fire fighters in Chile.

Their equipment and procedures closely resemble that of firefighting services in places such as the United States.  There are a few smaller towns that subscribe to a private firefighting force's services, but other than the method of financing, they are no different than the volunteer firefighting companies throughout the country.

# Results

## Financing and the "Free Rider Problem"

The national head of the national firefighters association ([Junta de Bomberos](http://www.bomberos.cl/)) estimated that [55% of fire service funding][bbc_article] was from the Chilean central government, and some of the remaining from local governments. The remainder was from donations.

*Requires evidence:* Chilean firefighting companies must procure their own equipment and training.  Many departments receive large enough quantities of donations from the local populations to sufficiently equip themselves for modern firefighting tactics.  Because the support of the firefighting companies is as equally voluntary as participation in the company itself, some people provide support while others do not.  This does not present a problem, despite the fact that there are people who do not contribute to the companies; the companies are as sufficiently funded as firefighting companies in subsidized areas.

## Competition

*Requires evidence:* On rare occasion, competing firefighting companies have been established in the same towns and cities due to dissatisfaction with the existing service.  More often than not, the new service has either co-existed with the more-established company for a long period of time, or absorbed the donations of the other company due to superior service.  Such a circumstance is impossible in a government subsidized environment.

# Sources
Many relevant sources are in Spanish. Work needs to be done to translate or find translated sources.

## References

* [Chile: fighting fires for free][global_post_article] (Global Post)
* [BBC Article][bbc_article] (including some criticism)
* [Santiago Times][santiago_times_article] article

## Other Resources
* [Wikipedia article](http://en.wikipedia.org/wiki/Geography_of_firefighting#Chile)

[global_post_article]: http://www.globalpost.com/dispatch/chile/100928/volunteer-firefighters
[santiago_times_article]: http://santiagotimes.cl/the-bomberos-chiles-volunteer-firefighting-force/
[bbc_article]: http://www.bbc.com/news/world-latin-america-27186801