<!-- title: Edhi Foundation (Pakistan) - Ambulance Service -->

<!-- redirect: /w/c/EmergencyServices/EmergencyMedical/edhi_foundation_pakistan_ambulance_service -->

The [Edhi foundation][edhi_home] is a volunteer run and funded charity organization in Pakistan. It was started by [Abdul Sattar Edhi][npr_article], a famous religious humanitarian. The Edhi Foundation provides many services for the poor. Among these is the [world's largest][dunyanews_article] private fleet of [Ambulances][edhi_ambulance].

During his life, Edhi received [criticism][guardian_article] from religious conservatives, and competition from extremists who have tried to set up charities to promote their violent jihadist organizations. He died at in 2016 at the age of 88, but his organization lives on.

# Incentives

The Edhi Foundation takes a [principled stance][edhi_major_features] against accepting assistance from any government:

> Edhi Foundation has set many examples, which can’t be traced by other NGOs and trusts actively serving in Pakistan that Edhi believes in the principle of self help, thus he is running all his social welfare driven activities without getting funding from any government or donor agency. It is a fact that Edhi isn’t entertaining even he refuses the concept of support from others. The only donations are entertained from the individuals and few leading businessmen.

[comment]: <> (TODO: Anything interesting about his motivations from [Vice][vice_interview] or [NPR][npr_article] (or maybe put this in opening paragraph))

# Results

Edhi commands the [world's largest][dunyanews_article] private ambulance service. According to their website, they command:

* [Street Ambulances][edhi_ambulance]: 1800 vehicles across Pakistan
* [Marine Ambulances][edhi_marine_ambulance]: 28 rescue boats
* [Air Ambulances][edhi_air_ambulance]: 2 aircraft and one helicopter

[comment]: <> (TODO: Find news sources so I don't have to trust them.)
[comment]: <> (TODO: How big is the government fleet? How effective are either?)

## Government police protection

Against his wishes, Abdul Sattar Edhi received [round-the-clock protection from police][vice_interview] because of threats from the Taliban.

# Sources

## Reference:

### Edhi Foundation pages:

* [Home page][edhi_home]
* [General informational page][edhi_major_features]
* [Ambulance informational page][edhi_ambulance]
* [Marine ambulance informational page][edhi_marine_ambulance]
* [Air ambulance informational page][edhi_air_ambulance]

### News sources:

* [NPR article][npr_article]
* [Guardian article][guardian_article]
* [Dunya News article][dunyanews_article] (Pakistani news source)
* [Vice Interview][vice_interview]

## Other Resources:

* [Wikipedia Article][wikipedia_article]

# See Also:

## Other services from Edhi Foundation

* [Edhi Foundation - Homes and Orphanage Centers](/w/c/SocialServices/affordable_housing/edhi_foundation_pakistan_homes_and_orphanage_cente/)
* [Edhi Foundation - Missing Persons Service](/w/c/EmergencyServices/missing_persons/edhi_foundation_pakistan_missing_persons_service/)

[edhi_home]: https://edhi.org/
[edhi_major_features]: https://edhi.org/major-features-of-edhi-foundation/
[edhi_ambulance]: https://edhi.org/ambulance-service/
[edhi_marine_ambulance]: https://edhi.org/ambulance-service/#1503395024079-1fe96922-9ee3
[edhi_air_ambulance]: https://edhi.org/ambulance-service/#1503394908390-1569fc7b-5b51

[npr_article]: http://www.npr.org/sections/parallels/2016/07/08/485279862/abdul-sattar-edhi-known-as-pakistans-mother-teresa-dies-at-88
[guardian_article]: https://www.theguardian.com/world/2015/apr/01/pakistan-charity-abdul-sattar-edhi-foundation-karachi?CMP=Share_iOSApp_Other
[dunyanews_article]: http://dunyanews.tv/en/Pakistan/381541-Edhi-air-ambulance-service-restored-after-three-ye

[wikipedia_article]: https://en.wikipedia.org/wiki/Edhi_Foundation
[vice_interview]: https://www.youtube.com/watch?v=XiMv95Z-bow