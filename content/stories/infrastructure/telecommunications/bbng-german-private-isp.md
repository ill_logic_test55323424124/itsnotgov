<!-- title: Bürger Breitband Netz (Citizens’ Broadband Network) - German private community ISP -->

<!-- redirect: /w/c/Infrastructure/Telecommunications/BbngGermanPrivateIsp -->

One proposed option for providing Internet access to cities and towns is to turn it into a [municipal utility](http://en.wikipedia.org/wiki/Municipal_broadband). Because of the large investment required to lay down the infrastructure, it is presumed that only a concentrated interest such as a privately held ISP or a municipality would be able to fund it. Since the infrastructure behind utilities is considered a natural monopoly, it is seen as preferable by some to choose a municipality, since it is democratically elected. Is there another way to address these concerns?

[TOC]

The inhabitants of the village of Löwenstedt, in northern Germany, could not get reasonable access to internet. In response, the inhabitants [bootstrapped their own ISP][rt_article] and now have some of the fastest internet access in the country.

The town was far enough away from major thoroughfares and had a small enough population that neither the existing ISPs nor government would perform the necessary actions to provide the inhabitants with modern internet access. The residents of Löwenstedt banded together to create Citizens' Broadband Network Company (also known as BBNG). [94% of the population][thelocal_article] invested in the company to get it off the ground, as well as residents of nearby villages. The company easily raised the funds necessary for laying fiber-optic cable from the nearby existing ISP.

# Results

## Overcoming the "Free Rider Problem"

Municipal Internet access is advocated by some because it is seen as the only practical way to get enough citizens to contribute funding to a public project. The equivalent without the municipal taxes would be donations by citizens in the hopes of achieving Internet access for themselves and their neighbors, which is susceptible to the [free rider problem](http://en.wikipedia.org/wiki/Free_rider_problem).

In this case, those in the village and neighboring towns who wished to see high-speed internet invested in the company and now see dividends on their investment. Those who did not invest pay full-price for the service, which then contributes to the dividends seen by the investors. Unlike the case of municipal fiber, those who didn't want the service never had to pay for it and aren't forced to receive it.

## Overcoming problems with monopoly

Municipal Internet is sometimes seen as a way to avoid a concentrated monopoly in the form of a traditional ISP. In this case, 94% of the population owns the lines, greatly dispersing control, particularly among those who would be affected by issues such as [monopolistic pricing](http://en.wikipedia.org/wiki/Monopoly_price).

## Slow adoption elsewhere

The company has hoped its model would spread to 59 neighboring towns. As of June 3, 2014, only [one other village](http://rt.com/news/163412-german-village-own-broadband/) had signed onto the network. According to an article in [The Local][thelocal_article], solidarity is high in the town of Löwenstedt. This could be one explanation for its exceptional success.

## Right of way

Even when infrastructure in a given city is privately owned, the right to build the infrastructure is usually controlled by the municipality. This allows the municipality to effectively grant privileges by allowing certain companies to build out infrastructure. It also may result in the [expropriation of privately held land](http://en.wikipedia.org/wiki/Eminent_domain) in order to lay down the cables.

The degree to which this is the case of BBNG is uncertain given the sources found thus far.

# Sources

## References

* Article on [RT][rt_article]
* Article on [The Local][thelocal_article] (English-language German news website)

## Further Reading

* [Official website](http://www.buergerbreitbandnetz.de/)

[rt_article]: http://rt.com/news/163412-german-village-own-broadband/
[thelocal_article]: http://www.thelocal.de/20140601/german-villagers-build-own-broadband-network

