<!-- title: Telecommunications -->

<!-- redirect: /w/c/Infrastructure/Telecommunications -->

Telecommunication service is often provided by private companies, albeit arguably with some privileges granted by the local governments. However it is usually considered impossible to compete with the physical infrastructure, such as the running telephone, coaxial, or fiber cables between the various service providers and homes. Thus, the infrastructure is usually owned, or heavily regulated by, the local government, to prevent an exploitative monopoly.

Recently, there has been the debate around [Network Neutrality](http://en.wikipedia.org/wiki/Net_neutrality), which is inherently linked to the topic of infrastructure monopoly. It is important to distinguish Network Neutrality as an outcome, from Network Neutrality legislation that seeks to create and maintain this outcome. Many proponents of Network Neutrality legislation would probably concede that, given sufficient alternatives to monopolistic [ISP](http://en.wikipedia.org/wiki/Internet_service_provider)s, Network Neutrality can be maintained naturally.

What alternatives have been built that have brought competition to telecom infrastructure? This may require some creativity! But, they do exist.