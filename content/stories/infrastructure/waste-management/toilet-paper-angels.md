<!-- title: Toilet Paper Angels -->

During the partial government shutdown in the United States that began in December of 2018, the National Parks Service [stopped providing][usatoday] maintenance services at national parks. This [has not stopped][usatoday] tourists from visiting the parks, resulting in un-serviced toilets and a pileup of trash. At Joshua Tree National Park in California, volunteers from the groups [Friends of Joshua Tree][friends_home] and [Cliffhanger Guides][cliffhanger_home], along with various others who joined, have stepped up, [volunteering to fill in][time] some of these gaps. [This includes][time] taking out trash, scrubbing (though [not emptying][friends_blog_post]) toilets, and refilling toilet paper. [According to Time.com][time], the volunteers have been dubbed by some as the "Toilet Paper Angels". (This is an ongoing story as of this writing, January 7, 2019).

The government shutdown [began on][usatoday] December 22, 2018. By January 2, 2019, the National Parks Service was [forced to close][nps] all of the campgrounds at Joshua Tree National Park due to full toilets (which volunteers say they are [not equipped][friends_blog_post] to service) and some illegal activity. However Park Superintendent David Smith [recognized the efforts][nps] of the volunteers in maintaining it in the meantime:

> I want to extend a sincere thanks to local businesses, volunteer groups, and tribal members who have done their best to assist in picking up litter and helping maintain campgrounds. This is no reflection on their efforts and the park is very fortunate to have a community that exhibits the kind of care and concern witnessed over the last week.

According to the call-to-action blog post from Friends of Joshua Tree, [other parts of the park][friends_blog_post] remained open and as of January 2, 2019 they remained committed to helping where they could.

# The Organizations

The Friends of Joshua Tree is a organization that [acts as a liaison][friends_about] between the rock climbing community and the National Park Service, who were at one time at odds with each other. They [claim to be][friends_donate] funded entirely by donations and event proceeds, though they also have a [sponsors page][friends_sponsors]. [Cliffhanger Guides][cliffhanger_home] is a rock climbing guide service that [charges fees][cliffhanger_rates].

[John Lauretig][time] from Friends of Joshua Tree said:

> I live and recreate at this park and I want this land to be as great as can be for everyone visiting and that seems to be the same motivation for people volunteering... They want to give back to the park and local community. A lot of these people are returnees and they’re more than happy to give back to this park.

# Sources

## References

* Friends of Joshua Tree website
[friends_home]: http://www.friendsofjosh.org
    * [Home Page][friends_home]
[friends_about]: http://www.friendsofjosh.org/about
    * [About Page][friends_about]
[friends_donate]: http://www.friendsofjosh.org/donate/donations/
    * [Donation Page][friends_donate]
[friends_sponsors]: http://www.friendsofjosh.org/sponsors/
    * [Sponsors Page][friends_sponsors]
[friends_blog_post]: http://www.friendsofjosh.org/the-situation-in-joshua-tree-national-park-update/
    * Volunteering call-to-action [blog post][friends_blog_post]

* Cliffhanger Guides website
[cliffhanger_home]: https://cliffhangerguides.com
    * [Home Page][cliffhanger_home]
[cliffhanger_rates]: https://cliffhangerguides.com/rates-reservations/
    * [Rates for services][cliffhanger_rates]

[usatoday]: https://www.usatoday.com/story/news/nation/2019/01/01/free-all-national-parks-overrun-garbage-other-bad-behavior/2456757002/
* [USA Today article][usatoday] about bad conditions at national parks during the late 2018, early 2019 government shutdown
[time]: http://time.com/5491755/volunteers-clean-national-parks-shutdown/
* [Time.com article][time] about the volunteer group dubbed the "Toilet Paper Angels"
[nps]: https://www.nps.gov/jotr/learn/news/park-campgrounds-closed-during-government-shutdown.htm
* [National Parks Service notice][nps] about closure of campgrounds at Joshua Tree National Park

## Other Sources

[youtube]: https://www.youtube.com/watch?v=AsFgUi6cnPI
* [AFP News video][youtube] about the volunteers at Joshua Tree National Park
