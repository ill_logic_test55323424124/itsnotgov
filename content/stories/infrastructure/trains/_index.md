<!-- title: Trains (Long Distance) -->

<!-- redirect: /w/c/Infrastructure/Trains -->

Trains are often considered to be a product that requires government funding, and sometimes government operation. Most of the United States rail infrastructure was created by companies who received government funding, and were given to government land grants. Most city rail systems are municipally run today.

However, there are historical examples of privately owned urban train systems, and long distance train infrastructure being created with virtually no government funding.