<!-- title: Conservation Land Trusts -->

<!-- redirect: /w/c/Infrastructure/land_resource_management/conservation_land_trusts -->

A [Conservation Land Trust][conservation_tools] is a organization, usually private and non-profit, which protects natural lands either by purchasing it or via an [easement][wikipedia_easement]. (See Also: [Deed Restrictions][deed_restrictions_itsnotgov])

# Incentives

## Dues paying membership

While it is of course difficult to determine their motivations, funding for conservation land trusts appears to come by and large from people who voluntarily donate. Information from one example organization can be found [here][the_trustees_financial] (though a more comprehensive source should be determined). Membership can come with [certain benefits][the_trustees_membership], such as admittance to parks and discounts on events.

## Tax Breaks

The United States federal government encourages otherwise private land conservation with the use of tax breaks.

One example today is a [tax break][tax_breaks] to encourage land owners to agree to [conservational easements][wikipedia_easement]. (It would be good to know how long this has been the case and how that has effected the rate of easements). Also, conservational land trusts are often (and perhaps generally?) held [tax-free][the_trustees_history].

# Results

# Acres Protected

According to the Land Trust Alliance, as of 2015 a total of [56 million acres][census] have been protected by land trusts in the United States. However, there are several caveats with regard to government involvement in these numbers. (see below)

For a broad comparison, the United States [National Park Service][national_park_service] protects "over 84 million" acres. The National Park Service is only one among [many government organizations][protected_areas] that protect land in the United States.

# Government Assistance

## Funding

Apparently, as of 2003 there has been increasing [government subsidies][washington_policy_center] involved in purchasing land and easements. (It would be good to get some numbers on this).

## Zoning

Some land trusts lobby the government to [zone][washington_policy_center] private land to reduce its market value, making it easier to purchase.

## Legal structures

Conservational Land Trusts can have special legal designations. For instance, one of the earliest ones, The Trustees of Reservations, was created by an [act of legislation][the_trustees_history] which allowed it to exist tax-free. (It's unclear if the legal designation has any other consequence)

## Reconveyance to government organizations

In some cases (for uncertain reasons), conservational land trusts opt to protect a significant amount of land by [reconveying it to government organizations][census].

## Property Rights

The caveat that applies to all stories related to land ownership is that the government is (as of now) responsible for protecting property rights.

# Source

## References

* Land Trust Alliance:
    * [2015 Census][census] on the number of acres of land protected by trusts in the United States
    * [Article][tax_breaks] about tax breaks in the United States for donating land
* The Trustees of Reservations - based in New England, one of the earliest private conservational land trusts in the United States
    * [Membership benefits][the_trustees_membership]
    * [Financial information][the_trustees_financial]
    * [History][the_trustees_history]
* [Article][conservation_tools] about conservation land trusts, from the Pennsylvania Land Trust Association.
* [National Park Service FAQ][national_park_service]
* [Land Trusts: Separating the Good from the Bad][washington_policy_center] - Washington Policy Center.
    * This source is a free-market think tank and should not be used to bolster the free market position here. However, it mentions some important problems worth mentioning.

## Other Resources

Wikipedia articles:

* Land Trusts generally, with a section on [Conservation Land Trusts][wikipedia_land_trust]
* [Conservational Easements][wikipedia_easement] (agreements with private land owners to leave all or a portion of their land protected).
* [Protected areas][protected_areas] (protected by the United States federal government).

[wikipedia_land_trust]: https://en.wikipedia.org/wiki/Land_trust#Conservation_land_trusts
[wikipedia_easement]: https://en.wikipedia.org/wiki/Conservation_easement
[census]: https://www.landtrustalliance.org/about/national-land-trust-census
[tax_breaks]: https://www.landtrustalliance.org/topics/taxes/income-tax-incentives-land-conservation
[conservation_tools]: https://conservationtools.org/guides/150-what-is-a-land-trust
[washington_policy_center]: https://www.washingtonpolicy.org/publications/detail/land-trusts-separating-the-good-from-the-bad
[national_park_service]: https://www.nps.gov/aboutus/faqs.htm
[the_trustees_financial]: http://www.thetrustees.org/about-us/financial-support/
[the_trustees_membership]: http://www.thetrustees.org/membership/membership-benefits/
[the_trustees_history]: http://www.thetrustees.org/about-us/history/
[protected_areas]: https://en.wikipedia.org/wiki/Protected_areas_of_the_United_States
[deed_restrictions_itsnotgov]: /regulation/land-use/deed-restrictions
