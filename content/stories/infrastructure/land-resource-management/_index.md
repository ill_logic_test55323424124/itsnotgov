<!-- title: Natural Resource Conservation -->

<!-- redirect: /w/c/Infrastructure/land_resource_management -->

In 1968, [Garrett Hardin][garrett_hardin] wrote his landmark essay, [The Tragedy of the Commons][tragedy_of_commons]. He argued that when any natural resource is shared among multiple people, each individual acting rationally will extract as much as possible, leading to the eventual collapse of the resource. The tragedy, according to Hardin, is that collective strategies for managing resources, while conceivable and available, do not emerge naturally. The emblematic example that he provides is of a grazing commons used by multiple cattle herders. Each herder earns a certain net profit from each additional cow she grazes on the commons, but each cow also has a cost: the degradation of the commons. The “tragedy” emerges from the fact that while the herder retains a 100% share of the profit from each additional cow added to the commons, the cost of degradation is shared among all herders.

Is this tragedy really inevitable? Political scientist and winner of the 2009 Nobel Prize in Economics [Elinor Ostrom][elinor_ostrom] famously challenged the universality of the Tragedy of the Commons. She and her colleagues argued that, in practice, we share resources with our families, friends, and communities all the time without defaulting to such individualistic modes of extraction. Moreover, pulling from the experiences of anthropologists, sociologists, ethnographers, and development scholars from around the world, she and her colleagues identified many specific examples of successful collective resource management that doesn’t require the solution that Hardin suggested were necessary: Namely, legitimized state coercion and state-enforced privatization. Instead, the key has turned out to be institutions, or formal and informal rules of natural resource use. The results have been successful natural resource governance based on open communication among users, collective action, social sanctions, and adaptive management.

This section will highlight some of these stories. It should be noted that these systems that have been developed involve utilizing community standards of one sort or another. Ostrom's focus in her research was not necessarily through a libertarian lens. Thus, the stories discussed here must examine certain factors, such as the enforcement mechanisms used, and how ownership of resources is doled out.

# See Also

* Video: Elinor Ostrom [describes her work][beyond_tragedy_of_commons] on the question of the Tragedy of the Commons.

[tragedy_of_commons]: http://science.sciencemag.org/content/162/3859/1243
[garrett_hardin]: https://en.wikipedia.org/wiki/Garrett_Hardin
[elinor_ostrom]: https://en.wikipedia.org/wiki/Elinor_Ostrom
[beyond_tragedy_of_commons]: https://www.youtube.com/watch?v=ByXM47Ri1Kc