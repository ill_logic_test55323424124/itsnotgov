<!-- title: Privacy Policy -->

<!-- redirect: /w/s/PrivacyPolicy -->

NOTE: This is all much more than is currently necessary. This is copied from when the site was dynamic. It is now 100% static, which means that a lot of this does not apply. This will be updated (hopefully) soon.

# Privacy Policy

We (the folks at ItsNotGov.org) would first like to thank [Automattic](http://automattic.com), the providers of [Wordpress](http://en.wordpress.com), for offering their Privacy Policy for others to copy and modify under the [Creative Commons Sharealike](http://creativecommons.org/licenses/by-sa/2.5/) license (which means you can copy this one likewise, but we wouldn’t particularly recommend it). Visit [here](http://automattic.com/privacy/) to see the current version of Automattic’s Privacy Policy. The Privacy Policy below was copied from their January 13, 2015 version.

ItsNotGov.org is a pretty simple website. It aims to get people to collaborate to generate articles. We may also have a message board in the future to aid in collaboration, and perhaps some public profiles. Beyond an email address, personal information isn’t particularly important for these purposes.

* We don’t ask you for personal information unless we truly need it.
* We don’t share your personal information with anyone except to comply with the law, develop our products, or protect our rights.
* We don’t store personal information on our servers unless required for the on-going operation of one of our services.

Daniel Krol (“Owner”) operates ItsNotGov.org. It is Owner’s policy to respect your privacy regarding any information we may collect while operating this website. Below is our privacy policy for this website (ItsNotGov.org), and does not pertain to any other website that may be run by Owner.

If you have questions about deleting or correcting your personal data please contact us [here](mailto:policy@itsnotgov.org).

[TOC]

## Website Visitors

Like most website operators, Owner collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Owner’s purpose in collecting non-personally identifying information on this website is to better understand how Owner’s visitors use this website. From time to time, Owner may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of this website.

Owner also collects potentially personally-identifying information like Internet Protocol (IP) addresses for visitors to this website. Owner only discloses visitor IP addresses under the same circumstances that he uses and discloses personally-identifying information as described below.

## Gathering of Personally-Identifying Information

Certain visitors to this website choose to interact with Owner in ways that require Owner to gather personally-identifying information. The amount and type of information that Owner gathers depends on the nature of the interaction. For example, we ask visitors who sign up for an account on ItsNotGov.org to provide a username and email address. Those who engage in transactions with Owner – by purchasing merchandise, for example – are asked to provide additional information, including as necessary the personal and financial information required to process those transactions. In each case, Owner collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor’s interaction with Owner. Owner does not disclose personally-identifying information other than as described below. And visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities.

## Aggregated Statistics

Owner may collect statistics about the behavior of visitors to this website. For instance, Owner may monitor the most popular articles on the ItsNotGov.org site. Owner may display this information publicly or provide it to others. However, Owner does not disclose personally-identifying information other than as described below.

## Protection of Certain Personally-Identifying Information

With the possible exception of third party services with their own privacy policies described below, Owner discloses potentially personally-identifying and personally-identifying information only to those of his employees, contractors and affiliated organizations that (i) need to know that information in order to process it on Owner’s behalf or to provide services available at this website, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using this website, you consent to the transfer of such information to them. Owner will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to his employees, contractors and affiliated organizations, as described above, and third party services with their own privacy policies described below, Owner discloses potentially personally-identifying and personally-identifying information only in response to a subpoena, court order or other governmental request, or when Owner believes in good faith that disclosure is reasonably necessary to protect the property or rights of Owner, third parties or the public at large. If you are a registered user of this website and have supplied your email address, Owner may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what’s going on with Owner and our products. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. Owner takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.

Owner may disclose potentially personally-identifying and personally-identifying information to the following third party services. Their use of such data is covered in each of their own policies:

* [Heroku](https://www.heroku.com/policy/privacy) - Provides web hosting, database, and other services for ItsNotGov.org. As such, Heroku will have access to all data transmitted by you to ItsNotGov.org.
* [Cloudflare](https://www.cloudflare.com/security-policy) - Provides SSL, DNS, and security enhancements. Cloudflare will have access to all data transmitted by you to ItsNotGov.org, though will not necessarily retain it. See Cloudflare's [privacy policy](https://www.cloudflare.com/security-policy) for more details. (This is possible because of the way SSL is confugured with Cloudflare. We use their Full SSL (Strict) configuration. See [here](https://support.cloudflare.com/hc/en-us/articles/200170416-What-do-the-SSL-options-Off-Flexible-SSL-Full-SSL-Full-SSL-Strict-mean-) for more information.)
* [Bugsnag](https://bugsnag.com/docs/privacy) - Is used for error reporting. This is relevant to any data that appears in an error report.
* [Mailgun](http://www.mailgun.com/privacy) - Is used for handling certain incoming and outgoing email communication associated with this site and the ItsNotGov.org domain.
* [Gmail](https://www.gmail.com/intl/en_us/mail/help/terms.html) for Google Apps - Is used for handling certain incoming and outgoing email communication, particularly manual correspondence, associated with this site and the ItsNotGov.org domain.
* [Papertrail](https://papertrailapp.com/info/terms) - Is used to store and search server logs generated by the site.
* [Expedited SSL](www.expeditedssl.com) - Has access to all data in the Heroku instance, as a requirement of having an automatic SSL certificate updating feature.
* [Google Analytics](http://www.google.com/policies/privacy/) - Will have access to potentially personally identifying information such as IP address, browser version, language preferences, etc. We will not send personally identifying information such as email address (it is in fact prohibited by Google Analytics' own [Terms of Service](http://www.google.com/analytics/terms/us.html)). However we make no claims about how Google may or may not correlate your use with personally identifying information already collected by Google. See [here](https://www.google.com/policies/privacy/partners/) for details on how Google uses Analytics data.

## Other Third Party Services

At certain points, ItsNotGov.org may have links to third party services which are used in conjunction with ItsNotGov.org, but with whom Owner discloses no potentially personally-identifying or personally-identifying information. For instance, there may be a Google Group or IRC channel to discuss edits, or an official ItsNotGov.org Twitter account. These are completely independent services; any information you provide to these services is covered by those services' own privacy policies, and are not covered by the privacy policy of ItsNotGov.org. Consult the policies of those third party services for more information. Any information you share with those services that connects you to your activity on ItsNotGov.org is your decision and responsibility.

## Cookies

A cookie is a string of information that a website stores on a visitor’s computer, and that the visitor’s browser provides to the website each time the visitor returns. Owner uses cookies to help Owner identify and track visitors, their usage of this website, and their website access preferences. ItsNotGov.org visitors who do not wish to have cookies for this website placed on their computers should set their browsers to refuse cookies before using this website, with the drawback that certain features of this website may not function properly without the aid of cookies.

## Business Transfers

If Owner, or substantially all of his assets, were acquired, or in the unlikely event that Owner goes out of business or enters bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of Owner may continue to use your personal information as set forth in this policy.

## Ads

We currently do not host any advertisements on this website. The current intention is to keep it that way, to maximize objectivity in the content. However, this is not set in stone, so the following is in case this changes, to avoid having to issue a revised Privacy Policy.

Ads appearing on our website may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by Owner and does not cover the use of cookies by any advertisers.

## Article History

All historical revisions of articles are stored on our servers, other than specific revisions which Owner may decide to delete (for certain violations of Terms of Service, for instance). Older revisions of articles may or may not be visible to users and non-user visitors at any given time, depending on when and if Owner decides to make them available as a feature.

## Privacy Policy Changes

Although most changes are likely to be minor, Owner may change his Privacy Policy for this website from time to time, and in Owner’s sole discretion. If we make changes that are material, we will let you know by posting on one of our blogs, or by sending you an email or other communication before the changes take effect. The notice will designate a reasonable period of time after which the new Privacy Policy will take effect. If you disagree with our changes, then you should stop using this website within the designated notice period. Your continued use of this website will be subject to the new Privacy Policy.
